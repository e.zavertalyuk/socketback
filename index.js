import express from 'express';
import http from 'http';
import { Server } from 'socket.io';

const app = express();
const server = http.createServer(app);
const port = process.env.PORT || 3000;

const activeSessions = new Set();

const io = new Server(server);

io.on('connection', (socket) => {
   console.log('Пользователь подключился');

   activeSessions.add(socket.id);

   io.emit('activeSessions', activeSessions.size);

   socket.on('disconnect', () => {
      console.log('Пользователь отключился');
      activeSessions.delete(socket.id);
      io.emit('activeSessions', activeSessions.size);
   });
});

app.get('/', (req, res) => {
   res.send('Привет, мир!');
});

server.listen(port, () => {
   console.log(`Сервер запущен на порту ${port}`);
});
